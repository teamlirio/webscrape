$(document).ready(function() {
	var previousQuery = "";
	var startedFetching = false;
	//var totalNumberOfResults;
	var searchDomain = "https://www.google.com.ph";
	var resultsArray = [];
	var arrayCtr = 0;

	$("#btnsearch").on('click', function() {

		// textbox value
		var query = $("#query").val();

		// replaces all whitespace with "+"
		var currentQuery = query.replace(" ", "+");

		// base url		
		var baseUrl =  searchDomain + "/search?q=" + currentQuery;
		var content = "";
		var parameter = {
				url : baseUrl,	
				EXECUTE : function fetchPage() {
					if (resultsArray.length > 0 && resultsArray.length < 10) {
						this.url = this.url + "&start=10";
					}
					console.log("FETCHING url: " + this.url);
					startedFetching = true;
					jsonlib.fetch(this.url, function(m) {
						document.getElementById('test').innerHTML = m.content;
						content = m.content;
					});
				}
		};

		var preloader = { run : function () {
			var wrapper = document.createElement("div");
			wrapper.setAttribute("id","loader-wrapper");

			var loader = document.createElement("div");
			loader.setAttribute("id", "loader");

			$("#loader-container").append(wrapper);
			$("#loader-wrapper").append(loader);
		}, 
		del : function() {
			$("#loader-wrapper").remove();
			$("#loader").remove();
		}
		};


		if(previousQuery != currentQuery) {
			resultsArray = [];
			arrayCtr = 0;
			$(".childResults").empty();			
			previousQuery = currentQuery;
			preloader.run();
			var id;

			getResultsInIntervals();

		} else {
			console.log("Current query is the same with the previous query.");
		}

		function getResultsInIntervals(callback) {
			id = setInterval(getResults, 10000);
		}

		function checkIfResultsAreComplete() {
			// i'm the callback
			console.log('getResults done!');

			console.log("Checking if results are not yet complete...");
			if (resultsArray.length > 0 && resultsArray.length < 10) {
				console.log("Less than 10 results. Need to fetch more.");
				id = setInterval(getResults, 10000);
			}
		}

		function getResults() {
			if (!startedFetching || typeof content == "undefined") {
				console.log("Content is undefined or this is the first fetch.");
				parameter.EXECUTE();

			} else if (areResultsReady(content)) {	
				preloader.del();
				scrapeData();
				clearInterval(id);
				startedFetching = false; // reset startedFetching flag				
			}
			$("#results").hide();
			$("#test").hide();
		}


		function areResultsReady(content) {
			if (content.length == 0) {
				console.log("Content is still empty, maybe still loading..");
				return false;
			}
			console.log("Results are ready!");
			return true;
		}

		function scrapeData() {

			$resultsDiv = $("#ires");
			$resultsDiv.find("div.g:has(img)").remove();
			var results = $resultsDiv.find("div.g");
			//resultsArray = [];

			//var arrayCtr = 0;
			for(var ctr = 0; ctr < results.length; ctr++) {
				var titleText = $(results[ctr]).find("h3.r a:first").text();

				if (titleText) {
					var titleLink = $(results[ctr]).find("h3.r a:first").attr('href');
					var urlText = $(results[ctr]).find("cite").text();
					var description = $(results[ctr]).find("div span.st").text();

					var searchResult = {titletext : titleText, 
							titlelink : titleLink,
							urltext : urlText,
							description : description
					};

					resultsArray[arrayCtr] = searchResult;
					arrayCtr++;
					//console.log(arrayCtr + "-" + searchResult.titletext);
				}
			}

			displayResults();
			//totalNumberOfResults = totalNumberOfResults + resultsArray.length;
			//console.log("Total number of results: " + totalNumberOfResults);
		}

		function displayResults() {
			if (resultsArray.length >= 10) {
				for(var ctr = 0; ctr < 10; ctr++) {
					var html_headline = "<div class='displayres'>" + "<h3><a href='"+ searchDomain + resultsArray[ctr].titlelink + "'>" + resultsArray[ctr].titletext + "</a></h3>";
					var html_link = "<span>" + resultsArray[ctr].urltext + "</span>";
					var html_desc = "<p>" + resultsArray[ctr].description + "</p>" +	"</div>";
					var html_all = html_headline + html_link + html_desc;
					$(".childResults").append(html_all);
				}
				terminate();
			} else if (resultsArray.length > 0 && resultsArray.length < 10) {
				console.log("Results are still less than 10, not gonna display them yet.");
				checkIfResultsAreComplete()
			} else if (resultsArray.length == 0) {
				console.log("No results found!");
				$(".childResults").append("No results found!");
			}
			console.log("Total number of results is: " + resultsArray.length);

		}
		function terminate() {
			console.log("Done.");
		}
		return false;
	});

	});