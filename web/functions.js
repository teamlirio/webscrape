function getResults() {
			if (!startedFetching || typeof content == "undefined") {
				parameter.EXECUTE(); // invoke first fetch

			} else if (areResultsReady(content)) {	
				preloader.del();
				scrapeData();
				clearInterval(id);
				startedFetching = false; // reset startedFetching flag
				if (totalNumberOfResults == 0) {
					console.log("No results found!");
					$(".childResults").append("No results found!");
				} else if (totalNumberOfResults > 0 && totalNumberOfResults < 10) {
					console.log("Less than 10 results. Need to fetch page 2.");
					var totalNumberOfResultsNeeded = 10 - totalNumberOfResults;
					fetchPage2(totalNumberOfResultsNeeded);
				}
			}
			$("#results").hide();
			$("#test").hide();
		}
		return false;
	});


	function areResultsReady(content) {
		if (content.length == 0) {
			//console.log("Content is still empty, maybe still loading..");
			return false;
		}
		//console.log("Results are ready!");
		return true;
	}

	function scrapeData() {

		$resultsDiv = $("#ires");
		$resultsDiv.find("div.g:has(img)").remove();
		var results = $resultsDiv.find("div.g");
		var resultsArray = [];

		var arrayCtr = 0;
		for(var ctr = 0; ctr < results.length; ctr++) {
			var titleText = $(results[ctr]).find("h3.r a:first").text();
			var titleLink = $(results[ctr]).find("h3.r a:first").attr('href');
			var urlText = $(results[ctr]).find("cite").text();
			var description = $(results[ctr]).find("div span.st").text();

			var searchResult = {titletext : titleText, 
					titlelink : titleLink,
					urltext : urlText,
					description : description
			};
			//console.log(ctr + "-" + searchResult.titletext);

			if (titleText) {
				resultsArray[arrayCtr] = searchResult;
				arrayCtr++;
			}
		}

		displayResults(resultsArray);
		console.log(resultsArray.length);
		totalNumberOfResults = totalNumberOfResults + resultsArray.length;
		//console.log("Total number of results: " + totalNumberOfResults);
	}

	function displayResults(result) {


		for(var ctr = 0; ctr < result.length; ctr++) {
			var html_headline = "<div class='displayres'>" + "<h3><a href='https://www.google.com.ph" + result[ctr].titlelink + "'>" + result[ctr].titletext + "</a></h3>";
			var html_link = "<span>" + result[ctr].urltext + "</span>";
			var html_desc = "<p>" + result[ctr].description + "</p>" +	"</div>";
			var html_all = html_headline + html_link + html_desc;
			$(".childResults").append(html_all);
		}
	}
