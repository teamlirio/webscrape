$(document).ready(function() {
	var previousQuery = "";
	var startedFetching = false;
	var searchDomain = "https://www.google.com.ph/";
	var resultsArray = [];
	var arrayCtr = 0;
	
	$("#btnsearch").on('click', function() {
		// textbox value
		var query = $("#query").val();

		// replaces all whitespace with "+"
		var currentQuery = query.replace(" ", "+");

		// base url		
		var baseUrl =  searchDomain + "search?q=" + currentQuery;
		var baseUrl2 =  baseUrl + "&start=10";
		
		var content = "";
		var content2 = "";
		
		var parameter = {	
				EXECUTE : function fetchPage(baseUrl) {
					console.log("FETCHING...");
					startedFetching = true;
					jsonlib.fetch(baseUrl, function(p1) {
						document.getElementById('page1').innerHTML = p1.content;
						content = p1.content;
					});
				}, 
				EXECUTE2 : function fetchPage2(baseUrl2) {
					console.log("FETCHING...");
					startedFetching = true;
					jsonlib.fetch(baseUrl2, function(p2) {
						document.getElementById('page2').innerHTML = p2.content;
						content2 = p2.content;
					});
				}
		};

		var preloader = { run : function () {
			var wrapper = document.createElement("div");
			wrapper.setAttribute("id","loader-wrapper");

			var loader = document.createElement("div");
			loader.setAttribute("id", "loader");

			$("#loader-container").append(wrapper);
			$("#loader-wrapper").append(loader);
		}, 
		del : function() {
			$("#loader-wrapper").remove();
			$("#loader").remove();
		}
		};

		if(previousQuery != currentQuery) {
			resultsArray = [];
			arrayCtr = 0;
			emptyDivs();			
			previousQuery = currentQuery;
			preloader.run();
			var id = setInterval(getResults, 10000);

		} else {
			console.log("Current query is the same with the previous query.");
		}

		function emptyDivs() {
			$(".childResults").empty();	
			$("#page1").empty();
			$("#page2").empty();
		}


		function getResults() {
			if (!startedFetching || typeof content == "undefined") {
				parameter.EXECUTE(baseUrl); // invoke first fetch

			} else if (areResultsReady(content)) {
				$("#ires").find("div.g:has(img)").remove();
				$("#ires").find("div.g:has(li)").remove();
				var results = $("#ires").find("div.g");
				console.log(results.length);
				if(results.length == 0) {
					preloader.del();
					noSearchResult();
					clearInterval(id);
					startedFetching = false;
				} else if(results.length < 10 && results.length > 0) {
					parameter.EXECUTE2(baseUrl2);
					if(areResultsReady(content2)) {
						console.log("page 2 loaded!");
						preloader.del();
						scrapeData();
						clearInterval(id);
						startedFetching = false;
					} 
				} else if(results.length > 10) {
					preloader.del();
					scrapeData();
					clearInterval(id);
					startedFetching = false;
				} else {
					preloader.del();
					scrapeData();
					clearInterval(id);
					startedFetching = false;
				}
			}

			$("#results").hide();
			$("#test").hide();
		}

		function noSearchResult() {
			$(".childResults").append("<h2> No Search Results found.</h2>");
		}

		function areResultsReady(content) {
			if (typeof content == "undefined") {
				return false;
			}
			if (content.length == 0) {
				return false;
			}
			return true;
		}

		function scrapeData() {
			$resultsDiv = $("#ires");
			var results = $("div.g");

			for(var ctr = 0; ctr < results.length; ctr++) {
				var titleText = $(results[ctr]).find("h3.r a:first").text();

				if (titleText) {
					var titleLink = $(results[ctr]).find("h3.r a:first").attr('href');
					var urlText = $(results[ctr]).find("cite").text();
					var description = $(results[ctr]).find("div span.st").text();

					var searchResult = {titletext : titleText, 
							titlelink : titleLink,
							urltext : urlText,
							description : description
					};

					resultsArray[arrayCtr] = searchResult;
					arrayCtr++;
				}
			}
			displayResults(resultsArray);
		}

		function displayResults(result) {
			for(var ctr = 0; ctr < 10; ctr++) {
				var html_headline = "<div class='displayres'>" + "<h3><a href='"+ searchDomain + result[ctr].titlelink + "'>" + result[ctr].titletext + "</a></h3>";
				var html_link = "<span>" + result[ctr].urltext + "</span>";
				var html_desc = "<p>" + result[ctr].description + "</p>" +	"</div>";
				var html_all = html_headline + html_link + html_desc;
				$(".childResults").append(html_all);
			}
		}

		return false;
	});
});